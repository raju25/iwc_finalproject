const express=require("express")
const app=express();
const dotenv=require("dotenv")
const request=require("request");
const path=require("path");
dotenv.config()
var bodyParser = require('body-parser')
app.use(bodyParser.urlencoded({ extended: false }))
const PORT=8000
console.log("Port Number :",PORT)
// parse application/json
app.use(bodyParser.json())
app.set("view engine","ejs");
app.set("views",path.join(__dirname,'views'));

app.get("/",(req,res)=>{
    res.render("index")
})

app.get("/brewery",(req,res)=>{
    res.render("brewery")
})
app.get("/drinkCocktail",(req,res)=>{
    let url='https://www.thecocktaildb.com/api/json/v1/1/filter.php?a=Alcoholic'
    request(url,function(err,response,body){
        if(response.statusCode==500){
            next(new AppError("Movie Not found"));
        }
        else if(!err&&response.statusCode!=500&&response){
             drinks=JSON.parse(body);
             drinks=drinks.drinks
             res.render("drinksCocktail",{drinks})
        }
      
       
    })

})
app.get("/drinkMocktail",(req,res)=>{
    let url= 'https://www.thecocktaildb.com/api/json/v1/1/filter.php?a=Non_Alcoholic'
    request(url,function(err,response,body){
        if(response.statusCode==500){
            next(new AppError("Movie Not found"));
        }
        else if(!err&&response.statusCode!=500&&response){
             drinks=JSON.parse(body);
             drinks=drinks.drinks
             res.render("drinksMocktail",{drinks})
        }
      
       
    })
})
app.post("/search",async(req,res)=>{
    let city=req.body.city;
    let url=`https://api.openbrewerydb.org/breweries?by_city=${city}`
    request(url,function(err,response,body){
        if(response.statusCode==500){
            next(new AppError("Movie Not found"));
        }
        else if(!err&&response.statusCode!=500&&response){
            var breweries=JSON.parse(body);
            res.render("breweries",{breweries})
        }
        
    })
   

})

app.post("/cocktail",(req,res)=>{
    let cocktail=req.body.findCocktail
    let drinks;
    let url=`https://www.thecocktaildb.com/api/json/v1/1/search.php?s=${cocktail}`
    request(url,function(err,response,body){
        if(response.statusCode==500){
            next(new AppError("Movie Not found"));
        }
        else if(!err&&response.statusCode!=500&&response){
             drinks=JSON.parse(body);
             drinks=drinks.drinks
             res.render("cocktail",{drinks})
        }
      
       
    })
   
})

app.listen(PORT,()=>{
    console.log("Server started on port:"+PORT)
})