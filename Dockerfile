# Use Node 14 as the base image
FROM node:14


COPY . /app

WORKDIR /app

RUN npm install
# Install the  packages specified by package.json into the container
CMD ["npm","start"]

# Set the program that is invoked upon container instantiation
